%define datool_with_testing 0

Name:		aops-ceres
Version:	v1.4.1
Release:	7
Summary:	An agent which needs to be adopted in client, it managers some plugins, such as gala-gopher(kpi collection), fluentd(log collection) and so on.
License:	MulanPSL2
URL:		https://gitee.com/openeuler/%{name}
Source0:	%{name}-%{version}.tar.gz

BuildRequires:  python3-setuptools
Requires:   python3-requests  python3-jsonschema python3-libconf
Requires:   python3-concurrent-log-handler dmidecode dnf-hotpatch-plugin >= v1.3.4 
Provides:   aops-ceres
Conflicts:  aops-agent
Patch0001:  0001-improve-status-judgement-and-hotupgrade-logic.patch
Patch0002:  0002-support-restore-grubby-if-kabi-check-failed.patch
Patch0003:  0003-remove-installed-rpm-if-unreboot-and-improve-log-for-rollback.patch
Patch0004:  0004-fix-cve-2021-33633.patch
Patch0005:  0005-override-list-file-method.patch
Patch0006:  0006-update-register-template-file.patch


%description
An agent which needs to be adopted in client, it managers some plugins, such as gala-gopher(kpi collection), fluentd(log collection) and so on.

%package -n dnf-hotpatch-plugin
Summary: dnf hotpatch plugin
Requires: python3-hawkey python3-dnf syscare >= 1.1.0

%description -n dnf-hotpatch-plugin
dnf hotpatch plugin, it's about hotpatch query and fix

%package -n da-tool
Summary: da-tool is a sampling and analysis tool for function delay.
BuildRequires:  gcc-c++ cmake make
Requires: perf

%description -n da-tool
da-tool is a sampling and analysis tool for function delay.

%prep
%autosetup -n %{name}-%{version} -p1


# build for aops-ceres
%py3_build

# build for da-tool
cd ./extra-tools/da-tool
mkdir build
cd build

%if 0%{?datool_with_testing}
%define da_cmake_test_args -DCMAKE_BUILD_TYPE=Debug -DWITH_DEBUG=ON
%else
%define da_cmake_test_args -DCMAKE_BUILD_TYPE=Release -DWITH_DEBUG=OFF
%endif

cmake .. %{?da_cmake_test_args}
make
cd ../../../

# install for aops-ceres
%py3_install

# install for aops-dnf-plugin
cp -r hotpatch %{buildroot}/%{python3_sitelib}/dnf-plugins/

# install for da-tool
mkdir -p ${RPM_BUILD_ROOT}%{_bindir}
mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}
install -b -m400 ./extra-tools/da-tool/conf/da-tool.conf       ${RPM_BUILD_ROOT}%{_sysconfdir}/
install -b -m500 ./extra-tools/da-tool/build/da-tool-analysis  ${RPM_BUILD_ROOT}%{_bindir}/
install -b -m500 ./extra-tools/da-tool/script/da-tool.sh       ${RPM_BUILD_ROOT}%{_bindir}/

%files
%doc README.*
%attr(0644,root,root) %{_sysconfdir}/aops/ceres.conf
%attr(0644,root,root) /opt/aops/register_example.json
%{python3_sitelib}/aops_ceres*.egg-info
%{python3_sitelib}/ceres/*
%{_bindir}/aops-ceres

%files -n dnf-hotpatch-plugin
%{python3_sitelib}/dnf-plugins/*

%files -n da-tool
%defattr (-, root, root)
%config(noreplace) %{_sysconfdir}/da-tool.conf
%attr(0500, root, root) %{_bindir}/da-tool.sh
%attr(0500, root, root) %{_bindir}/da-tool-analysis

%changelog
* Wed Jun 05 2024 wenxin<wenxin32@foxmail.com> - v1.4.1-7
- update register template file

* Wed Feb 07 2024 wenxin<wenxin32@foxmail.com> - v1.4.1-6
- override list file method

* Sun Feb 04 2024 wenxin<wenxin32@foxmail.com> - v1.4.1-5
- fix cve-2021-33633

* Fri Dec 22 2023 wangguangge<wangguangge@huawei.com> - v1.4.1-4
- support remove installed rpm if unreboot and improve log info for rollback operation

* Wed Dec 20 2023 wangguangge<wangguangge@huawei.com> - v1.4.1-3
- support restore default grubby boot kernel if kernel kabi check failed

* Mon Dec 18 2023 wangguangge<wangguangge@huawei.com> - v1.4.1-2
- improve task execution status code and hotupgrade logic of reinstalling active/accept hotpatch

* Mon Dec 18 2023 wangguangge<wangguangge@huawei.com> - v1.4.1-1
- update to v1.4.1
- update cve fix logic
- support rollback operation for kernel hotpatch/coldpatch

* Tue Dec 12 2023 wenxin<wenxin32@foxmail.com> - v1.4.0-2
- fix unboundlocalerror with variable name log

* Tue Dec 12 2023 wenxin<wenxin32@foxmail.com> - v1.4.0-1
- Change CVE rollback task to hotpatch remove
- update cve scan method and cve fix method

* Mon Dec 4 2023 liuchanggeng<liuchanggeng@huawei.com> - v1.3.4-12
- to maintain functional stability, da-tool only releases network related analysis
- it is not allowed to modify the configuration file of the da-tool

* Tue Nov 28 2023 liuchanggeng<liuchanggeng@huawei.com> - v1.3.4-11
- da-tool consider trace as valid data when first core lost scheduling information
- update document

* Mon Nov 27 2023 liuchanggeng<liuchanggeng@huawei.com> - v1.3.4-10
- add check of command parameters and configuration files
- handling scenarios with lost scheduling information
- update document

* Thu Nov 23 2023 liuchanggeng<liuchanggeng@huawei.com> - v1.3.4-9
- da-tool add usage
- da-tool check for duplicate configurations

* Tue Nov 21 2023 wenxin<wenxin32@foxmail.com> - v1.3.4-8
- update return log field of the cve fix func
- add specific error information

* Mon Nov 20 2023 liuchanggeng<liuchanggeng@huawei.com> - v1.3.4-7
- new patch for da-tool
- add summary report of schedswitch and delay
- modify method of mark invalid data and expanding valid data range
- adjusted the format of the output data

* Wed Nov 15 2023 wangguangge<wangguangge@huawei.com> - v1.3.4-6
- fix bug in test_hotpatch.py

* Tue Nov 14 2023 liuchanggeng<liuchanggeng@huawei.com> - v1.3.4-5
- update buildrequires of da-tool

* Tue Nov 14 2023 wangguangge<wangguangge@huawei.com> - v1.3.4-4
- add require information of dnf-hotpatch-plugin

* Tue Nov 14 2023 gongzhengtang<gong_zhengtang@163.com> - v1.3.4-3
- modify the regular expression of kernel filter

* Mon Nov 13 2023 wangguangge<wangguangge@huawei.com> - v1.3.4-2
- support kabi check for dnf-hotpatch-plugin

* Sun Nov 12 2023 liuchanggeng<liuchanggeng@huawei.com> - v1.3.4-1
- update to v1.3.4
- bugfix: fix the bug that when multiple kernel versions coexist, the correct highest version cannot be identified
- add query file list function
- add sub package da-tool

* Tue Sep 19 2023 wenxin<shusheng.wen@outlook.com> - v1.3.1-5
- update func about querying applied hotpatch info

* Tue Sep 19 2023 wenxin<shusheng.wen@outlook.com> - v1.3.1-4
- update method of querying fixed cves by dnf plugin

* Wed Sep 13 2023 wenxin<shusheng.wen@outlook.com> - v1.3.1-3
- add file sync func

* Wed Sep 13 2023 wenxin<shusheng.wen@outlook.com> - v1.3.1-2
- update func named set_hotpatch_status_by_dnf_plugin

* Mon Sep 11 2023 zhuyuncheng<zhu-yuncheng@huawei.com> - v1.3.1-1
- update rollback task logic, better returned log
- update status code and return None when installed_rpm or available_rpm is empty

* Wed Aug 30 2023 wenxin<shusheng.wen@outlook.com> - v1.3.0-3
- update query disk info func

* Tue Aug 29 2023 wenxin<shusheng.wen@outlook.com> - v1.3.0-2
- fix bug: repeated display of vulnerabilities fixed by hotpatch

* Tue Aug 29 2023 wenxin<shusheng.wen@outlook.com> - v1.3.0-1
- update vulnerability scanning method and vulnerability fix method

* Fri Jun 30 2023 wenxin<shusheng.wen@outlook.com> - v1.2.1-7
- update release

* Fri Jun 30 2023 gongzhengtang<gong_zhengtang@163.com> - v1.2.1-6
- Match the correctly applied hot patches

* Wed Jun 21 2023 wenxin<shusheng.wen@outlook.com> - v1.2.1-5
- update hostpatch info query func

* Fri Jun 09 2023 wenxin<shusheng.wen@outlook.com> - v1.2.1-4
- fix issue: cve fix result doesn't match log

* Fri Jun 02 2023 wenxin<shusheng.wen@outlook.com> - v1.2.1-3
- update cve scan and cve fix

* Thu Jun 01 2023 wenxin<shusheng.wen@outlook.com> - v1.2.1-2
- modify the return result when no hot patch is matched

* Tue May 23 2023 wenixn<shusheng.wen@outlook.com> - v1.2.1-1
- the client supports hot patch cve rollback

* Thu May 11 2023 wenixn<shusheng.wen@outlook.com> - v1.2.0-4
- fix hotpatch fail show succeed bug

* Tue May 9 2023 ptyang<1475324955@qq.com> - v1.2.0-3
- fix hotpatch fail show succeed bug

* Thu Apr 27 2023 wenixn<shusheng.wen@outlook.com> - v1.2.0-2
- fix shell command return error;update registe funciton

* Mon Apr 17 2023 wenixn<shusheng.wen@outlook.com> - v1.2.0-1
- change usage of the ceres, don't used it as a service
- update function: scan cve and fix cve
- udpate cve fix, support for hotpatch

* Fri Dec 23 2022 wenxin<shusheng.wen@outlook.com> - v1.1.0-4
- Handle when the http response result is not 200

* Wed Dec 07 2022 wenxin<shusheng.wen@outlook.com> - v1.1.0-3
- update cve fix

* Wed Dec 07 2022 wenxin<shusheng.wen@outlook.com> - v1.1.0-2
- modify args of register func, add register file template

* Fri Nov 25 2022 wenxin<shusheng.wen@outlook.com> - v1.1.0-1
- remove test cases that use the responses module

* Wed Nov 23 2022 wenxin<shusheng.wen@outlook.com> - v1.0.0-3
- remove test case: remove test case about register

* Wed Nov 23 2022 wenxin<shusheng.wen@outlook.com> - v1.0.0-2
- update register: add field os_version for register

* Tue Nov 22 2022 zhuyuncheng<zhuyuncheng@huawei.com> - v1.0.0-1
- Package init
